import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fruits/cart/cart_model.dart';
import 'package:fruits/cart/cart_page.dart';
import 'package:provider/provider.dart';

import 'routes.dart';


void main() {
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CartModel()),
      ],
      child: MyApp(),
     ),
  );
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        //home: ProductListPage(title: Strings.homeTitle),
        //routes: <String, WidgetBuilder> {
          //CartPage.routeName: (BuildContext context) => CartPage(),
        //},
        initialRoute: '/',
        onGenerateRoute: Routes.routesList,
      );
  }
}



