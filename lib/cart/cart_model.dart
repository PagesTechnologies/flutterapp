import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:fruits/product/product.dart';

class CartModel extends ChangeNotifier {


  final List<Product> _items = [];

  Map<int, Product> _mapProducts=Map<int, Product>();
  Map<int, int> _mapNumsOfProductst=Map<int, int>();

  void addTest(Product item) {
    _items.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }




  void add(Product item){
    if(!_mapProducts.containsKey(item.id)){
      _mapProducts.putIfAbsent(item.id, () => item);
      _mapNumsOfProductst.putIfAbsent(item.id, () => 1);
    }else{
      _mapNumsOfProductst.update(item.id, (val) => val+1);
    }
    notifyListeners();
  }

  void addNumOfProduct(int id){
    _mapNumsOfProductst.update(id, (val) => val+1);
    notifyListeners();
  }

  void removeNumOfProduct(int id){
    if( _mapNumsOfProductst[id]>=2){
      _mapNumsOfProductst.update(id, (val) => val-1);
    }
    notifyListeners();
  }

  void remove(int id){
    _mapProducts.remove(id);
    _mapNumsOfProductst.remove(id);
    notifyListeners();
  }

  void removeAll(){
    _mapProducts.clear();
    _mapNumsOfProductst.clear();
    notifyListeners();
  }

  //int get totalPrice => _mapProducts.length;
  double get totalPrice {
    double price=0.0;
    _mapNumsOfProductst.forEach((key, value) {
      price=price+_mapProducts[key].productPrice*value;
    });
    return price;
  }


  Map get numsOfProducts => _mapNumsOfProductst;
  List<Product> get productsList {
    List<Product> list = List<Product>();
    _mapProducts.forEach((key, value) =>list.add(_mapProducts[key]));
    return list;
  }

}