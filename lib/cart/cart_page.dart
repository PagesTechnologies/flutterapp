import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../strings.dart';
import 'package:provider/provider.dart';
import 'cart_model.dart';




class CartPage extends StatefulWidget {
  static const  routeName = '/cart';
  int productId;

  CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  String title;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.cartTitle),
        actions: [
          IconButton(
            icon: Icon(Icons.remove_shopping_cart),
            onPressed: (){
              context.read<CartModel>().removeAll();
            },
          ),
        ],
      ),
      body: Consumer<CartModel>(
          builder: (context, cart, child) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: cart.productsList.isEmpty
                  ? Center(child: Text(
                      '${Strings.cartEmpty}',
                      style: TextStyle(fontSize: 18,),
                    ))
                  :  Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      itemBuilder: (BuildContext context, int i){
                        return Row(
                          children: [
                            Text(
                              "${cart.productsList[i].productName}",
                              style: TextStyle(fontSize: 18,),
                            ),
                            Expanded(
                              child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.remove_circle_outline),
                                      onPressed: (){
                                        context.read<CartModel>().removeNumOfProduct(cart.productsList[i].id);
                                      },
                                    ),
                                    Text("${cart.numsOfProducts[cart.productsList[i].id]}"),
                                    IconButton(
                                      icon: Icon(Icons.add_circle_outline),
                                      onPressed: () {
                                        context.read<CartModel>().addNumOfProduct(cart.productsList[i].id);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: (){
                                context.read<CartModel>().remove(cart.productsList[i].id);
                              },
                            ),
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => const Divider(),
                      itemCount: cart.productsList.length,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(32.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "${Strings.cartAllPrice}:",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(fontSize: 18,),
                          ),
                          Text(
                            "${cart.totalPrice}",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(fontSize: 18, color: Colors.pink),
                          ),
                          Text(
                              "${Strings.currency}",
                              textDirection: TextDirection.ltr,
                          ),
                        ],
                      ),
                  ),
                ],
              ),
            );
          },
      ),
    );
  }

}