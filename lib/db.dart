import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'product/product.dart';

class Db{

  static Database _db;
  static Future<void> initDb() async{
    if(_db!=null){
      return;
    }

    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "product_database.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      print("БАЗА ДАННЫХ НЕ ОБНАРУЖЕНА");

      //Надо для правильного формирования пути к базе
      Database db = await openDatabase(
        join(await getDatabasesPath(), 'product_database.db'),
      );
      db.close();

      //copy db from assets to database folder
      ByteData data = await rootBundle.load("assets/product_database.db");
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes, flush: true);

      //Открываем скопированную базу
      _db = await openDatabase(
        join(await getDatabasesPath(), 'product_database.db'),
        onCreate: (db, version) {},
        onUpgrade: (Database db, int oldVersion, int newVersion){
          db.execute("ALTER TABLE products ADD COLUMN productFavorite INTEGER DEFAULT 0");
        },
        version: 5,
      );

    }else{
      _db = await openDatabase(
        join(await getDatabasesPath(), 'product_database.db'),
        onUpgrade: (Database db, int oldVersion, int newVersion){
          db.execute("ALTER TABLE products ADD COLUMN productFavorite INTEGER DEFAULT 0");
        },
        version: 6,
      );
    }

  }


  static Future<List<Product>> getProducts() async{
    await initDb();
    final List<Map<String, dynamic>> maps = await _db.query('products');
      return List.generate(maps.length, (i)  {
        return Product(
          id: maps[i]['id'],
          productName: maps[i]['productName'],
          productDesc: maps[i]['productDesc'],
          productPrice: maps[i]['productPrice'],
          productLogo: maps[i]['productLogo'],
        );
      });
  }

  static Future<Product> getProduct(int id) async{
    await initDb();
    final List<Map<String, dynamic>> item= await _db.query(
      'products',
      where: "id = ?",
      whereArgs: [id],
    );
    return  Product(id:item[0]['id'], productName:item[0]['productName'], productDesc:item[0]['productDesc'], productPrice:item[0]['productPrice'],  productFavorite:item[0]['productFavorite']);
  }

  static Future<List<String>> getImages(int id) async{
    await initDb();
    final List<Map<String, dynamic>> item= await _db.query(
      'product_images',
      where: "product_id = ?",
      whereArgs: [id],
    );
    return List.generate(item.length, (i)  {
        return item[i]['product_image'];
    });
  }

   static Future<void> addInFavorite(int id) async{
    await initDb();
    await _db.update(
        "products",
        {'productFavorite':1},
      where: "id = ?",
      whereArgs: [id],
    );
   }

  static Future<void> removeFromFavorite(int id) async{
    await initDb();
    await _db.update(
      "products",
      {'productFavorite':0},
      where: "id = ?",
      whereArgs: [id],
    );
  }



}