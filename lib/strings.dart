
class Strings{
  static const String pleaseWait='Пожалуйста подождите...';
  static const String homeTitle="Магазин свежих фруктов";
  static const String price="Цена";
  static const String currency=" руб.";

  static const String cartTitle="Корзина";
  static const String cartAllPrice="Общая стоимость";
  static const String cartEmpty="Корзина пуста";
}