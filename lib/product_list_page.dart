import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fruits/cart/cart_model.dart';
import 'package:provider/provider.dart';

import 'strings.dart';
import 'package:transparent_image/transparent_image.dart';
import 'db.dart';
import 'product/product.dart';
import 'product/product_page.dart';
import 'product/product_args.dart';

import 'dart:math';

class ProductListPage extends StatefulWidget {
  ProductListPage({Key key}) : super(key: key);

  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  Future<List<Product>> _productsList;
  Color _color = Colors.white60;

  @override
  void initState() {
    super.initState();
    _productsList=Db.getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.homeTitle),
        actions: [
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () => Navigator.pushNamed(context, '/cart'),
          ),
        ],
      ),
      body: Column(
        mainAxisSize:MainAxisSize.max,
        children: [
          Expanded(
            child:FutureBuilder<List<Product>>(
                future: _productsList,
                builder:  (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
                  Widget _child;
                  if (snapshot.hasData) {
                    _child =ListView.separated(
                      shrinkWrap: true,
                      padding: const EdgeInsets.all(8.0),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return  InkWell(
                            splashColor: Colors.green,
                            splashFactory: InkRipple.splashFactory,
                            onTap: () {
                              Navigator.pushNamed(context,ProductPage.routeName,arguments: ProductArgs(snapshot.data[index].id, snapshot.data[index].productName));
                            },
                              child: Row(
                                children: [
                                  ClipOval(
                                      child:Container(
                                        width: 50,
                                        height: 50,
                                        color: Colors.grey,
                                        child: FittedBox(
                                          child: FadeInImage.memoryNetwork(
                                            placeholder: kTransparentImage,
                                            image: snapshot.data[index].productLogo,
                                          ),
                                          fit: BoxFit.cover,
                                        ),
                                      )
                                  ),
                                  Expanded(
                                    child:Container(
                                      height: 50,
                                      margin: EdgeInsets.only(left: 8.0, right: 8.0),
                                      child: Center(
                                          child: Text(
                                            '${snapshot.data[index].productName}',
                                            style: TextStyle(fontSize: 18,),
                                            overflow: TextOverflow.ellipsis,
                                          )
                                      ),
                                    ),
                                  ),
                                  Text(
                                    '${snapshot.data[index].productPrice}',
                                    style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.pink,
                                    ),
                                  ),
                                  Text(
                                    Strings.currency,
                                    style: TextStyle(fontSize: 18,),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.add_shopping_cart),
                                    onPressed: () {
                                      context.read<CartModel>().add(snapshot.data[index]);
                                    },
                                  ),
                                ],
                              ),
                              /*
                              onTap: (){
                                Navigator.pushNamed(context,ProductPage.routeName,arguments: ProductArgs(snapshot.data[index].id, snapshot.data[index].productName));
                              }
                               */
                          );
                      },
                      separatorBuilder: (BuildContext context, int index) => const Divider(),
                    );
                  } else {
                    print(snapshot.data);
                    _child = Column(
                        children: [
                          SizedBox(
                            child: CircularProgressIndicator(),
                            width: 40,
                            height: 40,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text(Strings.pleaseWait),
                          ),
                        ]
                    );
                  }
                  return Center(
                    child: _child,
                  );
                }
            ),
          )
        ],
      ) ,
    );
  }
}

