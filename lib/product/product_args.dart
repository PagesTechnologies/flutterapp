class ProductArgs {
  final int id;
  final String title;
  ProductArgs(this.id, this.title);
}