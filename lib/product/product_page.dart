import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fruits/cart/cart_page.dart';
import '../strings.dart';
import 'product.dart';
import '../db.dart';
import 'images_carusel.dart';
import 'package:fruits/cart/cart_model.dart';
import 'package:provider/provider.dart';

class ProductPage extends StatefulWidget {
  static const routeName = '/product';
  int productId;
  String title;

  ProductPage({Key key, @required this.title,  @required this.productId}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  Product _product;
  bool _favColorTogle=false;


  @override
  void initState() {
    super.initState();
    _loadProductData(widget.productId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () => Navigator.pushNamed(context, CartPage.routeName),
          ),
        ],
      ),
      body: Column(
        mainAxisSize:MainAxisSize.max,
        children: [
          ImageCarusel(productId:widget.productId),
          Container(
            margin: const EdgeInsets.all(8),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.favorite,
                    color: _favColorTogle ? Colors.pink : Colors.grey,
                  ),
                  onPressed: (){
                    if(_favColorTogle){
                      _removeFav(widget.productId);
                    }else{
                      _addFav(widget.productId);
                    }
                    setState(() {
                      _favColorTogle=!_favColorTogle;
                    });
                  },
                ),
                Expanded(
                  child:Text(
                      Strings.price,
                    style: TextStyle(fontSize: 18,),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      _product?.productPrice.toString(),
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink,
                      ),
                    ),
                    Text(
                      Strings.currency,
                      style: TextStyle(fontSize: 18,),
                    ),
                    IconButton(
                      icon: Icon(Icons.add_shopping_cart),
                      onPressed: () {
                        context.read<CartModel>().add(_product);
                      },
                    ),
                  ],
                )
              ],
            ),
          ),

          Expanded(
            child:  SingleChildScrollView(
              child:Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(_product==null ? "" :_product.productDesc),
              ),
            ),
          )
        ],
      ) ,
    );
  }

  void _loadProductData(int id) async{
    Product _prd=await Db.getProduct(id);
    if (this.mounted){
      setState((){
        _product=_prd;
        _favColorTogle=_prd.productFavorite==1 ? true : false;
      });
    }
  }

  void _addFav(int id) async{
    await Db.addInFavorite(id);
  }

  void _removeFav(int id) async{
    await Db.removeFromFavorite(id);
  }

}