
class Product {
  final int id;
  final String productName;
  final String productDesc;
  final int productPrice;
  final String productLogo;
  final int productFavorite;

  Product({this.id, this.productName, this.productDesc, this.productPrice, this.productLogo, this.productFavorite=0});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productName': productName,
      'productDesc': productDesc,
      'productPrice': productPrice,
      'productLogo': productLogo,
      'productFavorite':productFavorite,
    };
  }



}