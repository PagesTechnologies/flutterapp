import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../db.dart';

class ImageCarusel extends StatefulWidget {
  final int productId;
  ImageCarusel({Key key, this.productId}) : super(key: key);
  @override
  _ImagesCaruselState createState() => _ImagesCaruselState();
}

class _ImagesCaruselState extends State<ImageCarusel> {
  List<Widget> _images=List();
  List<String> _imgURLs=List();
  int _current=0;
  double _height=200;

  final controller=PageController(
    initialPage: 0,
  );

  @override
  void initState() {
    super.initState();
    _getImageURLs(widget.productId);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
        height: _height,
        child: PageView(
            controller: controller,
            children: List.generate(_imgURLs.length, (i)  {
              return Stack(
                alignment: Alignment.center,
                children: [
                  CircularProgressIndicator(),
                  OverflowBox(
                    // OverflowBox передает размеры в FittedBox, Stack этого делать не может
                    minWidth: MediaQuery.of(context).size.width,
                    maxWidth: MediaQuery.of(context).size.width,
                    minHeight: _height,
                    maxHeight: _height,
                    child:  FittedBox(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: _imgURLs[i],
                      ),
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ],
              );
            }),
            onPageChanged: (i)=>{
              setState(() {
               _current = i;
              })
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _imgURLs.map((item) {
            int index = _imgURLs.indexOf(item);
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? Color.fromRGBO(0, 0, 0, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }

  void _getImageURLs( int id) async{
    List<String> imgURLs=await Db.getImages(id);
    /*
    List<Widget> imgs= List.generate(imgURLs.length, (i)  {
      return Stack(
        alignment: Alignment.center,
        children: [
          CircularProgressIndicator(),
          OverflowBox(
            // OverflowBox передает размеры в FittedBox, Stack этого делать не может
            minWidth: MediaQuery.of(context).size.width,
            maxWidth: MediaQuery.of(context).size.width,
            minHeight: _height,
            maxHeight: _height,
            child:  FittedBox(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: imgURLs[i],
              ),
              fit: BoxFit.fitWidth,
            ),
          ),
        ],
      );
    });

     */
    setState(() {
      _imgURLs=imgURLs;
      //_images=imgs;
    });
  }

}




