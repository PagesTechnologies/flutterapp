import 'package:flutter/material.dart';
import 'product_list_page.dart';
import 'product/product_page.dart';
import 'cart/cart_page.dart';
import 'product/product_args.dart';

class Routes{
  static Route<dynamic> routesList(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => ProductListPage());
      case '${CartPage.routeName}':
        return MaterialPageRoute(builder: (_) => CartPage());
      case '${ProductPage.routeName}':
       return MaterialPageRoute(builder: (_) {
         ProductArgs args = settings.arguments;
         return ProductPage(title:args.title,productId:args.id);
       });
      default:
        return null;
    }

  }
}